// import logo from './logo.svg';
import './App.css';  
import Header from './components/Header';
import Listing from './components/Listing';
import Footer from './components/Footer';

function App() {
  return (
  <><Header />
  <Listing/>
  <Footer/> </>

  );
}

export default App;
