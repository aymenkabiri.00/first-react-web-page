import im1 from "./image/bateau-1.png";
import im2 from "./image/bateau-2.jpeg";
import im3 from "./image/bateau-3.jpeg";
import im4 from "./image/bateau-4.jpg";
import im5 from "./image/bateau-5.jpeg";
import im6 from "./image/bateau-6.jpeg";
import "./Listing.css";


 function Listing(){
    return( 
            

<div class=" grid grid-cols-2 md:grid-cols-4 gap-4 ">
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image.jpg" alt="" />
        <br />
        <div className="prix"> <p> <b>hemp seed oil</b></p>
        <br /> <button>19$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-1.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b> sneakers</b></p>
        <br /> <button>70$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-2.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>white hand bag</b></p>
        <br /> <button>9,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-3.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>decoration baskets</b></p>
        <br /> <button>19$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-4.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>smart watch</b></p>
        <br /> <button>49,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-5.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>footsalle shoes</b></p>
        <br /> <button>49,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-6.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b> shower gel</b></p>
        <br /> <button>4,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-7.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>white hand bag</b></p>
        <br /> <button>9,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-8.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>decoration lamp</b></p>
        <br /> <button>19$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-9.jpg" alt="" />
        <br />
        <div className="prix">
        <p> <b>organic apple soap</b></p>
        <br /> <button>6,99$</button>
        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-10.jpg" alt="" />
        <br />
        <div className="prix ">
        <p> <b>remote xbox</b></p>
        <br /> <button>39$</button>

        </div>
    </div>
    <div>
        <img class="h-auto max-w-full rounded-lg" src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-11.jpg" alt="" />
        <br />
        <div className="prix">
            <p><b>leather bag </b> </p> <br />
            <button>22$</button>
        </div>
    </div>
</div>
)
}

export default Listing;